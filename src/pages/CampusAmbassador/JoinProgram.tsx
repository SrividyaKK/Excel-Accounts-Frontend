import React from "react";
import { signUpForAmbassador } from "../../store/Actions/ambassadorActions";
import { setSelected, setErrorMsg } from "../../store/Actions/setTC";
import { useDispatch, useSelector } from "react-redux";

const JoinProgram = () => {
  const { selected, errorMsg } = useSelector((state: any) => state.joinProgram);
  const dispatch = useDispatch();

  const toggleSelected = () => setSelected(dispatch);
  const handleJoin = () => {
    if (selected) {
      signUpForAmbassador(dispatch);
    } else {
      setErrorMsg(dispatch, "Pls agree to T&C");
    }
  };

  return (
    <div className="card fullCenter">
      <div className="card-body">
        <h4 className="card-title">
          Join the Campus Ambassador Program today!
        </h4>
        <div>...Terms and Conditions...</div>
        <div className="form-check">
          <label className="form-check-label">
            <input
              className="form-check-input"
              type="checkbox"
              value=""
              onClick={toggleSelected}
            />
            I agree to the Terms and Conditions
            <span className="form-check-sign">
              <span className="check"></span>
            </span>
          </label>
        </div>
      </div>
      {!!errorMsg && (
        <span className="tc" style={{ color: "red" }}>
          {errorMsg}
        </span>
      )}
      <button type="submit" className="btn btn-primary" onClick={handleJoin}>
        Join
      </button>
    </div>
  );
};

export default JoinProgram;
