const hostname =
  window.location.hostname === 'localhost'
    ? 'staging.accounts.excelmec.org'
    : window.location.hostname

export const WSRoot = `ws://${hostname}`

const eventsBase = 'events.excelmec.org'
const eventsGateway =
  process.env.REACT_APP_PROJECT === 'staging'
    ? `staging.${eventsBase}/api`
    : eventsBase

export const endpoints = {
  ApiRoot: `https://${hostname}/api`,
  EventsApiRoot: `https://${eventsGateway}`,
}
