import React from "react";

interface StatCardProps {
  title: number;
  desc: string;
  icon: string;
  tableId: string;
  icon_bg: string;
}

const StatCard = ({ title, desc, tableId, icon, icon_bg }: StatCardProps) => {
  const handleClick = () => {
    try {
      document.getElementById(tableId)!.scrollIntoView({ behavior: "smooth" });
    } catch {
      // couldn't scroll to position
    }
  };

  return (
    <div className="card card-stats custom-card ambassador-card" onClick={handleClick}>
      <div className={`card-header card-header-${icon_bg} card-header-icon`}>
        <div className="card-icon">
          <i className="material-icons">{icon}</i>
        </div>
        <p className="card-category">{desc}</p>
        <h3 className="card-title">{title}</h3>
      </div>
    </div>
  );
};

export default StatCard;
