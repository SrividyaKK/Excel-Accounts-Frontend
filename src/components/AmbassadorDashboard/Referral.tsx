import React from "react";
import { useSelector } from "react-redux";
import {
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
  TelegramShareButton,
  TelegramIcon,
} from "react-share";
import { selectAmbassadorId } from "../../store/Selectors/ambassadorSelectors";

const Referral = () => {
  const referralCode: string = useSelector(selectAmbassadorId);

  const copyToClipboard = () => {
    const copyText = document.getElementById(
      "ta-referral-code"
    ) as HTMLInputElement;
    copyText!.select();
    copyText.setSelectionRange(0, copyText.value.length);
    document.execCommand("copy");
  };

  const referral_url = `${window.location.origin}/auth/login?referral=${referralCode}`;
  const text = 'Check out: ';
  const subject = 'Excel 2020';

  return (
    <div className="col-md-6 col-lg-6">
      <div className="card">
        <img
          className="card-img-top referral-img"
          src={require("../../assets/img/gift.svg")}
          alt="Card image cap"
        />
        <div className="card-body">
          <div className="referral-card-text">
            <span className="referral-title">Refer Friends. Get Rewards. </span>
            <span>It's simple: </span>
            <ul>
              <li>You invite your friends</li>
              <li>They get $7 off</li>
              <li>
                You get 5 points for every user who joins and 20 points for
                every user upgrades to premium
              </li>
            </ul>
            <div className="referral-brands">
              <EmailShareButton url={referral_url} subject={subject} body={text}>
                <EmailIcon size={32} round={true} path="" />
              </EmailShareButton>
              <FacebookShareButton url={referral_url} quote={text}>
                <FacebookIcon size={32} round={true} path="" />
              </FacebookShareButton>
              <TwitterShareButton url={referral_url} title={text}>
                <TwitterIcon size={32} round={true} path="" />
              </TwitterShareButton>
              <WhatsappShareButton url={referral_url} title={text}>
                <WhatsappIcon size={32} round={true} path="" />
              </WhatsappShareButton>
              <TelegramShareButton url={referral_url} title={text}>
                <TelegramIcon size={32} round={true} path="" />
              </TelegramShareButton>
            </div>
            <span className="tc">Or share your personal referral code: </span>
            <div className="referral-link">
              <textarea readOnly rows={1} id="ta-referral-code" value={referralCode} />
              <button className="btn btn-primary" onClick={copyToClipboard}>
                <i className="material-icons">content_copy</i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Referral;
